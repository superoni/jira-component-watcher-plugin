/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.notification.type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.notification.type.AbstractNotificationType;
import com.atlassian.crowd.embedded.api.User;

import com.burningcode.jira.plugin.JIRAComponentWatcher;
/**
 * Notification type used in sending notifications to component watchers.
 * 
 * @author Ray Barham
 */
public class ComponentWatcherNotificationType extends AbstractNotificationType {
    protected static String DISPLAY_NAME = "Component Watchers";
    protected static String LABEL = "Component_Watchers";

    private final JIRAComponentWatcher jiraComponentWatcher;

    /**
     * Default Constructor.
     */
    public ComponentWatcherNotificationType() {
        super();
        
        jiraComponentWatcher = new JIRAComponentWatcher();
    }

    /**
     * Returns the name associated with this notification type.
     * 
     * @return The name of this notification type.
     */
    public String getDisplayName() {
        return ComponentWatcherNotificationType.DISPLAY_NAME;
    }
    
    /**
     * Returns a list of all the component watchers associated with the issue that threw the event.
     * 
     * @return The name of this notification type.
     */
    public List<NotificationRecipient> getRecipients(IssueEvent arg0, String arg1) {
        Collection<GenericValue> components = arg0.getIssue().getComponents();
        ArrayList<NotificationRecipient> recipients = new ArrayList<NotificationRecipient>();
        
        for(Iterator<GenericValue> i = components.iterator(); i.hasNext();)
        {
            GenericValue component = (GenericValue)i.next();
            Collection<User> usersWatching = jiraComponentWatcher.getAllUsersWatching((Long)component.get("id"));
            for(Iterator<User> j = usersWatching.iterator(); j.hasNext();)
            {
                NotificationRecipient recipient = new NotificationRecipient((User)j.next());
                if(!recipients.contains(recipient))
                    recipients.add(recipient);
            }
        }
        
        return recipients;
        
    }
    
    /**
     * Returns the label associated with this notification type.  The label is used in the database store and various other places.
     * @return The label of this notification type.
     */
    public static String getLabel() {
        return ComponentWatcherNotificationType.LABEL;
    }
}
