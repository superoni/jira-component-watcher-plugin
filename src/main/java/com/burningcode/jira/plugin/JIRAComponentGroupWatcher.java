/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.plugin;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.ofbiz.core.entity.GenericEntityException;

import webwork.action.ActionContext;

import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.web.component.multigrouppicker.GroupPickerWebComponent;
import org.apache.commons.lang.StringUtils;
import com.atlassian.crowd.embedded.api.Group;;

/**
 * Used to access and modify group watchers for a component.
 * 
 * This is the class that handles the retrieving, adding, and removing of
 * groups as watchers for the JIRA Component Watcher plug-in.
 * 
 * @author Ray Barham
 */
public class JIRAComponentGroupWatcher extends JIRAComponentAbstractWatcher<Group>
{
    private static final long serialVersionUID = 1L;
    private final GroupManager groupManager;

    /**
     * Default constructor for the class. 
     * @throws GenericEntityException 
     */
    public JIRAComponentGroupWatcher(GroupManager groupManager) throws GenericEntityException
    {
        super("componentGroupWatcher", "groupname");
        this.groupManager = groupManager;
    }

    /**
     * Used to return a Group object from the passed group name.  This method is required and is called by methods
     * inherited from the JIRAComponentAbstractWatcher class.
     * 
     * @param watcher The group name of the watcher.
     * @return Group object
     */
    protected Group castWatcher(String watcher)
    {
    	if(groupManager.groupExists(watcher))
    	{
    		return groupManager.getGroupObject(watcher);
    	}
    	
    	// Remove the watcher from the component watcher if it no longer exists.
    	if(this.getPropertySet().exists(watcher))
    		this.getPropertySet().remove(watcher);
    	
    	return null;
    }

    /**
     * Used to cast a list of group names to a list of Group objects.  This method is required and is called by methods
     * inherited from the JIRAComponentAbstractWatcher class.
     * 
     * @param watcherList List of group names as GenericValues.
     * @return List of Group objects.
     */
    protected Collection<Group> castWatchers(Collection<String> watcherList)
    {
        Vector<Group> castWatchers = new Vector<Group>();

        for(Iterator<String> i = watcherList.iterator(); i.hasNext();)
        {
            String groupName = (String)i.next();
            Group group = (Group)this.castWatcher(groupName);

            if(group != null)
            {
                castWatchers.add(group);
            }
        }

        return castWatchers;
    }

    /**
     * Called when groups are requested to be added as watchers.
     * 
     * @return The status of the execution.  "Success" is always returned unless the remote user does not have permissions which "securitybreach" is returned instead
     * @throws DataAccessException
     * @throws com.atlassian.jira.bc.EntityNotFoundException
     */
    public String doAddWatchers() throws DataAccessException, com.atlassian.jira.bc.EntityNotFoundException
    {
        if(!this.hasPermissions())
            return "securitybreach";

        Map<?, ?> params = ActionContext.getParameters();
        if(params.containsKey("groupWatchers[]")){
	        String[] watchers = (String[])params.get("groupWatchers[]");
	        Collection<String> groupNamesToAdd = GroupPickerWebComponent.getGroupNamesToAdd(StringUtils.join(watchers, ","));
	
	        this.addWatchers(groupNamesToAdd);
        }

        return SUCCESS;
    }

    /**
     * Called when groups are requested to be removed as watchers.
     * 
     * @return The status of the execution.  "Success" is always returned unless the remote user does not have permissions which "securitybreach" is returned instead
     * @throws DataAccessException
     * @throws com.atlassian.jira.bc.EntityNotFoundException
     */
    public String doRemoveWatchers() throws DataAccessException, com.atlassian.jira.bc.EntityNotFoundException
    {
        if(!this.hasPermissions())
            return "securitybreach";

        Vector<String> groupNamesToRemove = new Vector<String>(GroupPickerWebComponent.getGroupNamesToRemove(ActionContext.getParameters(), REMOVE_WATCHER_PREFIX));

        this.removeWatchers(groupNamesToRemove);

        return SUCCESS;
    }
    
    public Collection<Group> getGroupsToAdd()
    {
    	Collection<Group> currentGroups = this.getWatchers();
    	Collection<Group> groupsToAdd = groupManager.getAllGroups();
    	groupsToAdd.removeAll(currentGroups);
    	return groupsToAdd;
    }
}