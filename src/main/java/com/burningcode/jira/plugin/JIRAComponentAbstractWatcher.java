/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.plugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;

/**
 * This is an abstract class that is used to facilitate the adding, removing, and retrieving of watchers
 * from the JIRA database.
 * 
 * @author Ray Barham
 * @see com.atlassian.jira.web.action.JiraWebActionSupport
 */
public abstract class JIRAComponentAbstractWatcher<T> extends JiraWebActionSupport
{
    private static final long serialVersionUID = 1L;
    protected static final String REMOVE_WATCHER_PREFIX = "removeWatcher_";

    private Long componentId;
    private String watcherField; // The value stored in the watcherField textarea found in the JIRAComponentWatcherControl.vm.
    private String entityName;    // The object name used to store data to the database (i.e., JIRAComponentUserWatcher)
    private String watcherFieldEntity;     // The field name used to store the watcher (i.e., username, groupname)

    /**
     * Constructor for this JIRAComponentAbstractWatcher class.
     * 
     * @param entityName Entity name used in writing to the database.
     * @param watcherFieldEntity  The type of watcher.  This serves no purpose at present but should correspond to the type of watcher the inherited class adds.
     * @throws GenericEntityException
     */
    public JIRAComponentAbstractWatcher(String entityName, String watcherFieldEntity) throws GenericEntityException
    {
        this.entityName = entityName;
        this.watcherFieldEntity = watcherFieldEntity;

        setWebResources();
    }

    /**
     * Called when watchers are requested to be added.
     * 
     * @param watchersToAdd List of watcher names to add.
     */
    public void addWatchers(Collection<String> watchersToAdd)
    {
        Collection<T> currWatchers = (Collection<T>) this.getWatchers();
        Vector<String> invalidWatchers = new Vector<String>();

        for(Iterator<String> i = watchersToAdd.iterator(); i.hasNext();)
        {
            String watcher = (String)i.next();
            T castWatcher = this.castWatcher(watcher);

            String watcherName = null;
            if(castWatcher instanceof User)
            	watcherName = ((User)castWatcher).getName();
            else if(castWatcher instanceof Group)
            	watcherName = ((Group)castWatcher).getName();

            if(watcherName != null)
            {
                if(!currWatchers.contains(castWatcher))
                {
                	assignWatcher(watcherName);
                }

                else
                {
                	this.addErrorMessage(
                		getText("action.jiracomponentwatcher.watchers.error.dup", new String[]{watcher})
                	);

                    invalidWatchers.add(watcher);
                }
            }

            else
            {
            	this.addErrorMessage(
            		getText("action.jiracomponentwatcher.watchers.error.invalid", new String[]{watcher})
            	);

                invalidWatchers.add(watcher);
            }
        }

        this.setWatcherField(StringUtils.join(invalidWatchers.toArray(), ","));
    }
    
    
    /**
     * Assigns a watcher to a component
     * @param watcherName
     */
    protected void assignWatcher(String watcherName)
    {
    	this.getPropertySet().setString(watcherName, this.watcherFieldEntity);
    }

    /**
     * Abstract method called when a watcher name is needed to be cast to an object.
     * 
     * @param watcher Name of the watcher.
     * @return The watcher cast as an object.
     */
    protected abstract T castWatcher(String watcher);

    /**
     * Abstract method called when a list of watcher names is needed to be cast to a list of objects.
     * 
     * @param watcherList List of watcher names.
     * @return List of cast watchers.
     */
    protected abstract Collection<T> castWatchers(Collection<String> watcherList);

    /**
     * Abstract method called when the user chooses to add a watcher.
     * 
     * Here is an example of the URL when this method is called from within velocity...
     * http://localhost:8080/secure/project/EditComponentUserWatcher!addWatchers.jspa?componentId=10000
     * @return The status of adding the watchers. Returns "securitybreach" if user does not have permission to add watchers.
     * @throws DataAccessException 
     * @throws EntityNotFoundException
     * @throws com.opensymphony.user.EntityNotFoundException 
     */
    public abstract String doAddWatchers() throws DataAccessException, EntityNotFoundException, EntityNotFoundException;

    /**
     * Default action performed.
     * 
     * @return The status of the actions.  Returns "securitybreach" if user does not have permission to add watchers.
     * @throws Exception
     */
    public String doDefault() throws Exception
    {
        if (this.hasPermissions())
            return super.doDefault();
        else
            return "securitybreach";
    }

    /**
     * Action performed on execute.
     * 
     * @return The status of the actions.  Returns "securitybreach" if user does not have permission to add watchers.
     * @throws Exception
     */
    protected String doExecute() throws Exception
    {
        if (this.hasPermissions())
            return super.doExecute();
        else
            return "securitybreach";
    }

    /**
     * Abstract method called when the user chooses to remove a watcher.
     * 
     * Here is an example of the URL when this method is called from within velocity...
     * http://localhost:8080/secure/project/EditComponentUserWatcher!addWatchers.jspa?componentId=10000
     * @return The status of removing the watchers. Returns "securitybreach" if user does not have permission to add watchers.
     * @throws DataAccessException 
     * @throws EntityNotFoundException 
     */
    public abstract String doRemoveWatchers() throws DataAccessException, EntityNotFoundException;

    /**
     * Returns the ProjectComponent object of the current component id.
     * 
     * @return ProjectComponent object of the current component Id.
     * @throws EntityNotFoundException 
     */
    public ProjectComponent getComponent() throws EntityNotFoundException
    {
        return ComponentManager.getInstance().getProjectComponentManager().find(this.getComponentId());	
    }

    /**
     * Returns the current component id
     * 
     * @return The current component id.
     */
    public Long getComponentId()
    {
        return this.componentId;
    }

    /**
     * Returns a PropertySet object used for access/modifying the component watchers.
     * @return The PropertySet where the component watchers are stored.
     */
    public PropertySet getPropertySet()
    {
        HashMap<String, Object> args = new HashMap<String, Object>();
        args.put("delegator.name", "default");
        args.put("entityName", this.entityName + "_" + componentId);
        args.put("entityId", new Long(1));

        return PropertySetManager.getInstance("ofbiz", args);
    }

    /**
     * Returns a StringUtils object used by the velocity template.
     * 
     * Note: This was only added because the stringUtils parameter did not seem available already.
     * 
     * @return StringUtils object.
     */
    public StringUtils getStringUtils()
    {
        return new StringUtils();
    }

    /**
     * Gets the name of the field that the watchers are entered in.
     * 
     * @return Name of the field being used.
     */
    public String getWatcherField()
    {
        return this.watcherField;
    }

    /**
     * Returns a list of all the watchers. 
     * 
     * Note: This method calls the castWatchers method so insure that that method is properly defined before using.
     * 
     * @return Returns a list of all the watchers as objects.
     */
    @SuppressWarnings("unchecked")
	public Collection<T> getWatchers()
    {
        return this.castWatchers(this.getPropertySet().getKeys());
    }

    /**
     * Used to check if the user (remote user) has admin permissions.
     * 
     * @return True if the user has permissions, false otherwise.
     */
    protected boolean hasAdminPermission()
    {
    	return ComponentManager.getInstance().getPermissionManager().hasPermission(
    			Permissions.ADMINISTER,
    			ComponentManager.getInstance().getJiraAuthenticationContext().getLoggedInUser());
    }

    /**
     * Used to check if the user (remote user) has admin or project admin permissions.
     * 
     * @return True if the user has permissions, false otherwise.
     * @throws DataAccessException
     * @throws EntityNotFoundException
     */
    protected boolean hasPermissions() throws DataAccessException, EntityNotFoundException
    {
        if (hasProjectAdminPermission() || hasAdminPermission())
            return true;
        else
            return false;
    }

    /**
     * Used to check if the user (remote user) has project admin permissions.
     * 
     * @return True if the user has permissions, false otherwise.
     * @throws DataAccessException
     * @throws EntityNotFoundException
     */
    protected boolean hasProjectAdminPermission() throws DataAccessException, EntityNotFoundException
    {
        Project project = (Project) ComponentManager.getInstance().getProjectManager().getProjectObj(this.getComponent().getProjectId());
        return ComponentManager.getInstance().getPermissionManager().hasPermission(
        		Permissions.PROJECT_ADMIN,
    			project,
    			ComponentManager.getInstance().getJiraAuthenticationContext().getLoggedInUser());
    }

    /**
     * Called when watchers are requested to be removed.
     * 
     * @param watchersToRemove List of watcher names to remove.
     */
    //public abstract void removeWatchers(Collection<String> watchersToRemove);
    public void removeWatchers(Collection<String> watchersToRemove)
    {
        for(Iterator<String> i = watchersToRemove.iterator(); i.hasNext();)
        {
            String watcher = (String)i.next();
            Object castWatcher = this.castWatcher(watcher);
            
            String watcherName = null;
            if(castWatcher instanceof User)
            	watcherName = ((User)castWatcher).getName();
            else if(castWatcher instanceof Group)
            	watcherName = ((Group)castWatcher).getName();
            
            if(this.getPropertySet().exists(watcher))
            	this.getPropertySet().remove(watcher);
            else if(this.getPropertySet().exists(watcherName))
            	this.getPropertySet().remove(watcherName);
        }
    }

    /**
     * Sets the current component id
     * 
     * @param componentId A valid component id.
     */
    public void setComponentId(Long componentId)
    {
        this.componentId = componentId;
    }

    /**
     * Sets the name of the field that the watchers are entered in.
     * 
     * @param fieldName The name of the field used. 
     */
    public void setWatcherField(String fieldName)
    {
        this.watcherField = fieldName;
    }

    /**
     * Sets the web resources used.
     */
    protected void setWebResources() {
        WebResourceManager webResourceManager = ComponentManager.getInstance().getWebResourceManager();
        webResourceManager.requireResource("jira.webresources:autocomplete");
    }
}